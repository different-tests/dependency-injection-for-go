# Dependency Injection For Go

Base on https://locxngo.medium.com/dependency-injection-for-go-google-wire-vs-uber-dig-6154ae7dab3f


Dependency Injection (DI) is a software design pattern used in object-oriented programming and software engineering to achieve the principle of Inversion of Control (IoC). It is a technique that helps manage the dependencies between different components or classes in a more flexible and maintainable way. The primary goal of Dependency Injection is to promote loose coupling between components, making your code more modular, testable, and easier to maintain.